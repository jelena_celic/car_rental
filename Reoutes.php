<?php

/**
 * Ova datoteka vraca niz asocijativnih nizova koji predstavljaju rute koje
 * postoje u ovoj aplikaciji.
 * <pre>
 * Svaka ruta je asocijativni niz koji mora da sadrzi indekse:
 *  - Pattern    - Regularni izraz koji treba da odgovara zahtevu da se ruta izvrsi
 *  - Controller - Ime kontrolera koji treba koristiti za odgovor zahtevu.
 *                 Ako je ime kontrolera Main, ime klase je MainController.
 *                 Kao vrednost ovog indeksa asocijatvinog niza ide samo Main,
 *                 a ne MainController kako je puno ime klase.
 *  - Method     - Ime metoda izabranog kontrolera koji treba izvrsiti za
 *                 odgovor na pristigli zahtev koji odgovara ovoj ruti.
 * </pre>
 * 
 * Primer:
 * <pre><code>
 * return [
 *   [
 *     'Pattern'    => '|^login/?$|',
 *     'Controller' => 'Main',
 *     'Method'     => 'login'
 *   ],
 *   [
 *     'Pattern'    => '|^logout/?$|',
 *     'Controller' => 'Main',
 *     'Method'     => 'logout'
 *   ],
 *   [ # Poslednja ruta koja ce se izvrsiti ako ni jedna pre ne odgovara pristiglom zahtevu.
 *     'Pattern'    => '|^.*$|',
 *     'Controller' => 'Main',
 *     'Method'     => 'index'
 *   ]
 * ];
 * <code></pre>
 */
return [
    [
        'Pattern' => '|^login/?$|',
        'Controller' => 'Main',
        'Method' => 'login'
    ],
      [
        'Pattern' => '|^indexAdmin/?$|',
        'Controller' => 'Main',
        'Method' => 'indexAdmin'
    ],
    [
        'Pattern' => '|^contact/?$|',
        'Controller' => 'Main',
        'Method' => 'contact'
    ],
    [
        'Pattern' => '|^pricelist/?$|',
        'Controller' => 'Main',
        'Method' => 'pricelist'
    ],
    [
        'Pattern' => '|^aboutRent/?$|',
        'Controller' => 'Main',
        'Method' => 'aboutRent'
    ],
    [
        'Pattern' => '|^carRent/?$|',
        'Controller' => 'Main',
        'Method' => 'carRent'
    ],
     [
        'Pattern' => '|^carView/?$|',
        'Controller' => 'Main',
        'Method' => 'carView'
    ],
    [
        'Pattern' => '|^logout/?$|',
        'Controller' => 'Main',
        'Method' => 'logout'
    ],
    [
        'Pattern' => '|^admin/locations/?$|',
        'Controller' => 'AdminLocation',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^admin/locations/add/?$|',
        'Controller' => 'AdminLocation',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^admin/locations/edit/([0-9]+)/?$|',
        'Controller' => 'AdminLocation',
        'Method' => 'edit'
    ],
    [
        'Pattern' => '|^admin/categories/?$|',
        'Controller' => 'AdminCarCategory',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^admin/categories/add/?$|',
        'Controller' => 'AdminCarCategory',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^admin/categories/edit/([0-9]+)/?$|',
        'Controller' => 'AdminCarCategory',
        'Method' => 'edit'
    ],
    [
        'Pattern' => '|^admin/tags/?$|',
        'Controller' => 'AdminTag',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^admin/tags/add/?$|',
        'Controller' => 'AdminTag',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^admin/tags/edit/([0-9]+)/?$|',
        'Controller' => 'AdminTag',
        'Method' => 'edit'
    ],
    [
        'Pattern' => '|^admin/cars/?$|',
        'Controller' => 'AdminCar',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^admin/cars/add/?$|',
        'Controller' => 'AdminCar',
        'Method' => 'add'
    ],
    [
        'Pattern' => '|^admin/cars/edit/([0-9]+)/?$|',
        'Controller' => 'AdminCar',
        'Method' => 'edit'
    ],
    [
        'Pattern' => '|^admin/images/car/([0-9]+)/?$|',
        'Controller' => 'AdminCarImage',
        'Method' => 'listCarImages'
    ],
    [
        'Pattern' => '|^admin/images/car/([0-9]+)/add/?$|',
        'Controller' => 'AdminCarImage',
        'Method' => 'uploadImage'
    ],
    [
        'Pattern' => '|^category/([a-z0-9\-]+)/?$|',
        'Controller' => 'Main',
        'Method' => 'listByCategory'
    ],
    [
        'Pattern' => '|^car/([a-z0-9\-]+)/?$|',
        'Controller' => 'Main',
        'Method' => 'car'
    ],
    [
        'Pattern' => '|^([0-9]+)?/?$|',
        'Controller' => 'Main',
        'Method' => 'index'
    ],
    [
        'Pattern' => '|^search/?$|',
        'Controller' => 'Main',
        'Method' => 'search'
    ],
    [ # Poslednja ruta!
        'Pattern' => '|^.*$|',
        'Controller' => 'Main',
        'Method' => 'index'
    ]
];
