<?php
/**
 * Osnovna klasa svih modela
 */
abstract class Model implements ModelInterface {
    final protected static function getTableName() {
        return substr(strtolower(preg_replace('/[A-Z]/', '_$0', get_called_class())), 1, -6);
    }

    final public static function getAll() {
        $tableName = static::getTableName();
        $SQL = 'SELECT * FROM ' . $tableName . ';'; # ORDER BY ...
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    final public static function getById($id) {
        $tableName = static::getTableName();
        $SQL = 'SELECT * FROM ' . $tableName . ' WHERE ' . $tableName . '_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        if ($prep->execute([$id])) {
            return $prep->fetch(PDO::FETCH_OBJ);
        } else {
            return FALSE;
        }
    }

    final public static function add(array $data = []) {
        $nizImena     = [];
        $nizUpitnika  = [];
        $nizVrednosti = [];

        $tableName = static::getTableName();

        foreach ($data as $key => $value) {
            if (!preg_match('/^[a-z_][a-z0-9_]*$/', $key) or
                $key == $tableName . '_id' or is_array($value) or is_object($value)) {
                continue;
            }

            $nizImena[]     = '`' . $key . '`';
            $nizUpitnika[]  = '?';
            $nizVrednosti[] = $value;
        }

        $spisakPolja = implode(', ', $nizImena);
        $spisakUpitanika = implode(', ', $nizUpitnika);

        $SQL = "INSERT INTO `{$tableName}` ({$spisakPolja}) VALUES ({$spisakUpitanika});";
        $prep = DataBase::getInstance()->prepare($SQL);
        if ($prep) {
            $res = $prep->execute($nizVrednosti);
            if ($res) {
                return DataBase::getInstance()->lastInsertId();
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    final public static function edit($id, array $data = []) {
        $nizIzmena = [];
        $nizVrednosti = [];

        $tableName = static::getTableName();

        foreach ($data as $key => $value) {
            if (!preg_match('/^[a-z_][a-z0-9_]*$/', $key) or
                $key == $tableName . '_id' or is_array($value) or is_object($value)) {
                continue;
            }

            $nizIzmena[]    = '`' . $key . '` = ?';
            $nizVrednosti[] = $value;
        }

        $spisakIzmena = implode(', ', $nizIzmena);

        $SQL = "UPDATE {$tableName} SET {$spisakIzmena} WHERE {$tableName}_id = ?;";
        $prep = DataBase::getInstance()->prepare($SQL);
        if ($prep) {
            $nizVrednosti[] = $id;
            return $prep->execute($nizVrednosti);
        } else {
            return FALSE;
        }
    }

    final public static function delete($id) {
        $tableName = static::getTableName();
        $SQL = "DELETE FROM {$tableName} WHERE {$tableName}_id = ?;";
        $prep = DataBase::getInstance()->prepare($SQL);
        if ($prep) {
            return $prep->execute([$id]);
        } else {
            return FALSE;
        }
    }
}
