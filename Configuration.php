<?php

/**
 * Ova klasa predstavlja centralno mesto gde se nalaze svi vazni konfiguracioni
 * parametri ove veb aplikacije, koji su definisani kao clanovi podaci konstante.
 */
final class Configuration {

    const DB_HOST = 'localhost';
    const DB_USER = 'jelena';
    const DB_PASS = 'jelena';
    const DB_BASE = 'car_rental';
    const BASE = 'http://localhost/Car_Rental/';
    const PATH = '/Car_Rental/'; # Na normalnom sajtu to ce biti samo /
    const SALT = '5321651cb13brcashasdcvxcbxz23rt34trqo3ic';
    const IMAGE_DATA_PATH = 'data/images/';
    const ITEMS_PER_PAGE = 3;

}
