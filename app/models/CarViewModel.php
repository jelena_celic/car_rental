<?php

/**
 * Model koji odgovara tabeli car_view
 */
class CarViewModell implements ModelInterface {

    /**
     * Metod koji vraca spisak svih pregleda putnickih vozila poredjanih po datumu u opadajucem poretku
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM car_view ORDER BY `datetime` DESC;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima pregleda putnickih vozila(objekata) ciji car_view_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car_view WHERE car_view_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metod vraca niz objekata sa podacima o pregledima putnickim vozilima ciji ID broj je dat kao argument
     * @param int $id Id broj putnickog vozila
     * @return array
     */
    public static function getByCarId($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car_view WHERE car_id = ? ORDER BY `datetime` DESC;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

}
