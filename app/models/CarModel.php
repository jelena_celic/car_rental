<?php

/**
 * Model koji odgovara tabeli car
 */
class CarModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih putnickih vozila (objekata) poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM car ORDER BY `title`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Ovaj metod vraca spisak putnickih vozila iz baze i koristi sistem paginacije.
     * Stranica $page se koristi da se izracuna pocetni indeks rezultata odakle pocinje
     * priprema zapisa koji ce biti vraceni, a zavisi od konstante Configuration::ITEMS_PER_PAGE.
     * @param int $page
     * @return array
     */
    public static function getAllPaged($page) {
        $page = max(0, $page);
        $first = $page * Configuration::ITEMS_PER_PAGE;
        $SQL = 'SELECT * FROM car ORDER BY `title` LIMIT ?, ?';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEMS_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima putnickog vozila (objekta) ciji car_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metod vraca niz objekata sa podacima o tagovima koji su dodeljeni putnickom vozilu ciji ID broj je proslednjen kao argument metoda
     * @param int $car_id ID broj putnickog vozila
     * @return array
     */
    public static function getTagsForCarId($car_id) {
        $car_id = intval($car_id);
        $SQL = 'SELECT * FROM car_tag WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$car_id]);
        $spisak = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($spisak as $item) {
            $list[] = TagModel::getById($item->tag_id);
        }
        return $list;
    }

    /**
     * Metod vraca niz objekata sa podacima putnickih vozila koji spadaju pod kategoriju ciji ID broj je dat kao argument
     * @param int $id Id broj kategorije putnickog vozila
     * @return array
     */
    public static function getCarsByCarCategoryId($id) {
        $SQL = 'SELECT * FROM car WHERE car_category_id = ? ORDER BY `title`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * metod koji vraca objekat sa podacima putnickog vozila ciji slug je dat kao argument metoda
     * @param string $slug
     * @return stdClass
     */
    public static function getBySlug($slug) {
        $SQL = 'SELECT * FROM car WHERE slug = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$slug]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metod vrsi unos nove stavke u bazi podataka za putnicko vozilo
     * @param type $title
     * @param type $slug
     * @param type $short_text
     * @param type $long_text
     * @param type $price
     * @param type $location_id
     * @param type $car_category_id
     * @return int|boolean ID broj  putnickog vozila ili FALSE u slucaju greske
     */
    public static function add($title, $slug, $short_text, $long_text, $price, $location_id, $car_category_id, $user_id) {
        $SQL = 'INSERT INTO car (title, slug, short_text, long_text, price, location_id, car_category_id, user_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$title, $slug, $short_text, $long_text, $price, $location_id, $car_category_id, $user_id]);
        if ($res)
            return DataBase::getInstance()->lastInsertId();
        else
            return false;
    }

    /**
     * Metod menja sadrzaj zapisa u bazi za izabrano putnicko vozilo
     * @param type $id
     * @param type $title
     * @param type $slug
     * @param type $short_text
     * @param type $long_text
     * @param type $price
     * @param type $location_id
     * @param type $car_category_id
     * @return boolean
     */
    public static function edit($id, $title, $slug, $short_text, $long_text, $price, $location_id, $car_category_id) {
        $SQL = 'UPDATE car SET title = ?, slug = ?, short_text = ?, long_text = ?, price = ?, location_id = ?, car_category_id = ? WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$title, $slug, $short_text, $long_text, $price, $location_id, $car_category_id, $id]);
    }

    /**
     * Metod dodeljuje odabrani tag u odabrano putnicko vozilo
     * @param int $car_id
     * @param int $tag_id
     * @return boolean
     */
    public static function addTagToCar($car_id, $tag_id) {
        $SQL = 'INSERT INTO car_tag (car_id, tag_id) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$car_id, $tag_id]);
    }

    /**
     * Brise sve tagove koji su bili dodeljeni ovom putnickom vozilu
     * @param int $car_id
     * @return boolean|int
     */
    public static function delateAllTags($car_id) {
        $SQL = 'DELETE FROM car_tag WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$car_id]);
    }

    /**
     * Metod vraca spisak slika za odredjeno putnicko vozilo
     * @param int $car_id
     * @return array
     */
    public static function getCarImages($car_id) {
        $SQL = 'SELECT * FROM image WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$car_id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod vraca rezultate pretrage po datim kriterijumima.
     * @param int $location_id
     * @param int $car_category_id
     * @param array $tag_ids
     * @return array
     */
    public static function homePageSearch($location_id, $car_category_id, $tag_ids) {
        if ($location_id != -1 and $car_category_id != -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * FROM car ' .
                    'WHERE location_id = ? AND car_category_id = ? AND car_id IN (SELECT car_id FROM car_tag WHERE tag_id IN (' . $tag_placeholder_string . ')) ' .
                    'ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $location_id,
                $car_category_id
            ];
            $niz = array_merge($niz, $tag_ids);
            $prep->execute($niz);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id == -1 and $car_category_id != -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * ' .
                    'FROM car ' .
                    'WHERE car_category_id = ? AND car_id IN (' .
                    'SELECT car_id FROM car_tag WHERE tag_id IN (' . $tag_placeholder_string . ')' .
                    ') ' .
                    'ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $car_category_id
            ];
            $niz = array_merge($niz, $tag_ids);
            $prep->execute($niz);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id != -1 and $car_category_id == -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * ' .
                    'FROM car ' .
                    'WHERE location_id = ? AND car_id IN (' .
                    'SELECT car_id FROM car_tag WHERE tag_id IN (' . $tag_placeholder_string . ')' .
                    ') ' .
                    'ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $location_id
            ];
            $niz = array_merge($niz, $tag_ids);
            $prep->execute($niz);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id == -1 and $car_category_id == -1 and count($tag_ids) > 0) {
            $tag_placeholders = [];
            for ($i = 0; $i < count($tag_ids); $i++) {
                $tag_placeholders[] = '?';
            }
            $tag_placeholder_string = implode(', ', $tag_placeholders);

            $SQL = 'SELECT * ' .
                    'FROM car ' .
                    'WHERE car_id IN (' .
                    'SELECT car_id FROM car_tag WHERE tag_id IN (' . $tag_placeholder_string . ')' .
                    ') ' .
                    'ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $prep->execute($tag_ids);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id != -1 and $car_category_id != -1 and count($tag_ids) == 0) {
            $SQL = 'SELECT * FROM car WHERE location_id = ? AND car_category_id = ? ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $location_id,
                $car_category_id
            ];
            $prep->execute($niz);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id == -1 and $car_category_id != -1 and count($tag_ids) == 0) {
            $SQL = 'SELECT * FROM car WHERE car_category_id = ? ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $car_category_id
            ];
            $prep->execute($niz);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id != -1 and $car_category_id == -1 and count($tag_ids) == 0) {
            $SQL = 'SELECT * FROM car WHERE location_id = ? ORDER BY `title`';
            $prep = DataBase::getInstance()->prepare($SQL);
            $niz = [
                $location_id
            ];
            $prep->execute($niz);
            return $prep->fetchAll(PDO::FETCH_OBJ);
        }

        if ($location_id == -1 and $car_category_id == -1 and count($tag_ids) == 0) {
            return self::getAll();
        }
    }

}
