<?php

/**
 * Model koji odgovara tabeli location
 */
class LocationModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih lokacija poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM location ORDER BY `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima lokacije ciji image_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $image_id = intval($id);
        $SQL = 'SELECT * FROM location WHERE location_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$image_id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vrsi dodavanje zapisa lokacije u bazu podataka
     * @param string $name
     * @param string $slug
     * @return int|boolen ID broj zapisa u bazi ako je kreiran ili false ako je doslo do greske
     */
    public static function add($name, $slug) {
        $SQL = 'INSERT INTO location (name, slug) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $slug]);
        if ($res) {
            return DataBase::getInstance()->lastInsertId();
        } else {
            return false;
        }
    }

    /**
     * Metod koji vrsi izmenu zapisa lokacije u bazi podataka
     * @param int $id
     * @param string $name
     * @param string $slug
     * @return boolean
     */
    public static function edit($id, $name, $slug) {
        $SQL = 'UPDATE location SET name = ?, slug = ? WHERE location_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $slug, $id]);
    }

}
