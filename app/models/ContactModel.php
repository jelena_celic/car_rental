<?php

/**
 * Model koji odgovara tabeli contact
 */
class ContactModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih korisnika (objekata) poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM contact ORDER BY `first_name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Ovaj metod vraca spisak korisnika iz baze i koristi sistem paginacije.
     * Stranica $page se koristi da se izracuna pocetni indeks rezultata odakle pocinje
     * priprema zapisa koji ce biti vraceni, a zavisi od konstante Configuration::ITEMS_PER_PAGE.
     * @param int $page
     * @return array
     */
    public static function getAllPaged($page) {
        $page = max(0, $page);
        $first = $page * Configuration::ITEMS_PER_PAGE;
        $SQL = 'SELECT * FROM contact ORDER BY `contact_id` LIMIT ?, ?';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEMS_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima korisnika (objekta) ciji contact_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM contact WHERE contact_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
  /**
     * Metod dodaje sadrzaj zapisa u bazi za izabranog korisnika
     * @param type $id
     * @param type $contact_id
     * @param type $first_name
     * @param type $last_name
     * @param type $email
     * @param type $phone
     * @param type $message
     * @return boolean
     */
    public static function add($contact_id, $first_name, $last_name, $email, $phone, $message) {
        $SQL = 'INSERT INTO contact (contact_id, first_name, last_name, email, phone, message) VALUES (?, ?, ?, ?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$contact_id, $first_name, $last_name, $email, $phone, $message]);
        if ($res)
            return DataBase::getInstance()->lastInsertId();
        else
            return false;
    }

    /**
     * Metod menja sadrzaj zapisa u bazi za izabranog korisnika
     * @param type $id
     * @param type $contact_id
     * @param type $first_name
     * @param type $last_name
     * @param type $email
     * @param type $phone
     * @param type $message
     * @return boolean
     */
    public static function edit($id, $contact_id, $first_name, $last_name, $email, $phone, $message) {
        $SQL = 'UPDATE contact SET contact_id = ?, first_name = ?, last_name= ?, email = ?, phone = ?, message = ? WHERE contact_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$contact_id, $first_name, $last_name, $email, $phone, $message]);
    }

}
