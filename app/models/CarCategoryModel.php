<?php
/**
 * Model koji odgovara tabeli car_category
 */
class CarCategoryModel implements ModelInterface {
 /**
     * Metod koji vraca spisak svih kategorija putnickih vozila poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM car_category ORDER BY `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
 /**
     * Metod koji vraca objekat sa podacima putnickog vozila (objekata) ciji car_category_id je dat kao argeemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car_category WHERE car_category_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
     /**
     * Metod koji vraca objekat sa podacima putnickog vozila (objekata) ciji slug je dat kao argument metoda
     * @param string $slug
     * @return stdClass|NULL
     */
    public static function getBySlug($slug) {
        $SQL = 'SELECT * FROM car_category WHERE slug = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$slug]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    public static function getByCarCategoryId($id) {
        $SQL = 'SELECT * FROM car WHERE car_category_id = ? ORDER BY `title`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
   /**
     * Metod koji vrsi dodavanje zapisa kategorije u bazu podataka
     * @param string $name
     * @param string $slug
     * @return int|boolen ID broj zapisa u bazi ako je kreiran ili false ako je doslo do greske
     */
    public static function add($name, $slug) {
        $SQL = 'INSERT INTO car_category (name, slug) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $slug]);
        if ($res) {
            return DataBase::getInstance()->lastInsertId();
        } else {
            return false;
        }
    }
 /**
     * Metod koji vrsi izmenu zapisa kategorije u bazi podataka
     * @param int $id
     * @param string $name
     * @param string $slug
     * @return boolean
     */
    public static function edit($id, $name, $slug) {
        $SQL = 'UPDATE car_category SET name = ?, slug = ? WHERE car_category_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $slug, $id]);
    }

}
