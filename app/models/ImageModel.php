<?php

/**
 * Model koji odgovara tabeli image
 */
class ImageModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih slika poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM image ORDER BY `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima slika ciji image_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM image WHERE image_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metod vraca niz objekata sa podacima slika koje pripadaju odredjenom putnickom vozilu
     * @param int $id
     * @return array
     */
    public static function getByCarId($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM image WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod dodaje sliku za izabrano putnicko vozilo sa datom lokacijom slike
     * @param string $path
     * @param int $car_id
     * @return boolean
     */
    public static function add($path, $car_id) {
        $SQL = 'INSERT INTO image (path, car_id) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$path, $car_id]);
    }

}
