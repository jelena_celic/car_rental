<?php

/**
 * Model koji odgovara tabeli cenovnik
 */
class PricelistModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih vozila (objekata) poredjanih po imenu
     * @return array
     */
   public static function getAll() {
        $SQL = 'SELECT * FROM car ORDER BY `title`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }


    /**
     * Ovaj metod vraca spisak korisnika iz baze i koristi sistem paginacije.
     * Stranica $page se koristi da se izracuna pocetni indeks rezultata odakle pocinje
     * priprema zapisa koji ce biti vraceni, a zavisi od konstante Configuration::ITEMS_PER_PAGE.
     * @param int $page
     * @return array
     */
     public static function getAllPaged($page) {
        $page = max(0, $page);
        $first = $page * Configuration::ITEMS_PER_PAGE;
        $SQL = 'SELECT * FROM car ORDER BY `title` LIMIT ?, ?';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEMS_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
     

    /**
     * Metod koji vraca objekat sa podacima vozila (objekta) ciji car_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
       public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car WHERE car_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

}