<?php

/**
 * Model koji odgovara tabeli car_rent
 */
class CarRentModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih putnickih vozila (objekata) poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM car_rent ORDER BY `date_from` DESC;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Ovaj metod vraca spisak putnickih vozila iz baze i koristi sistem paginacije.
     * Stranica $page se koristi da se izracuna pocetni indeks rezultata odakle pocinje
     * priprema zapisa koji ce biti vraceni, a zavisi od konstante Configuration::ITEMS_PER_PAGE.
     * @param int $page
     * @return array
     */
    public static function getAllPaged($page) {
        $page = max(0, $page);
        $first = $page * Configuration::ITEMS_PER_PAGE;
        $SQL = 'SELECT * FROM car_rent ORDER BY `date_from` DESC LIMIT ?, ?';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$first, Configuration::ITEMS_PER_PAGE]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima putnickog vozila (objekta) ciji car_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car_rent WHERE car_rent_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    
    public static function add($car_id, $location_id, $date_from, $date_to, $phone, $email) {
        $SQL = 'INSERT INTO car_rent (car_id, location_id, date_from, date_to, phone, email) VALUES (?, ?, ?, ?, ?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$car_id, $location_id, $date_from, $date_to, $phone, $email]);
        if ($res)
            return DataBase::getInstance()->lastInsertId();
        else
            return false;
    }

    /**
     * Metod menja sadrzaj zapisa u bazi za izabrano putnicko vozilo
     * @param type $id
     * @param type $title
     * @param type $slug
     * @param type $short_text
     * @param type $long_text
     * @param type $price
     * @param type $location_id
     * @param type $car_category_id
     * @return boolean
     */
    public static function edit($id, $car_id, $location_id, $date_from, $date_to, $phone, $email) {
        $SQL = 'UPDATE car_rent SET car_id = ?, location_id = ?, date_from = ?, date_to = ?, email = ?, phone = ? WHERE car_rent_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$car_id, $location_id, $date_from, $date_to, $phone, $email, $id]);
    }
}
