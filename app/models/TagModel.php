<?php

/**
 * Model koji odgovara tabeli tag
 */
class TagModel implements ModelInterface {

    /**
     * Metod koji vraca spisak svih tagova poredjanih po imenu
     * @return array
     */
    public static function getAll() {
        $SQL = 'SELECT * FROM tag ORDER BY `name`;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute();
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }

    /**
     * Metod koji vraca objekat sa podacima taga ciji tag_id je dat kao arguemnt metoda
     * @param int $id
     * @return stdClass|NULL
     */
    public static function getById($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM tag WHERE tag_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }

    /**
     * Metod vraca niz objekata koji sadrze podatke o putnickim vozilima koji su obelezeni tagom ciji ID broj je dat kao argument metoda
     * @param int $id Id broj taga
     * @return array
     */
    public static function getCarsForTagId($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM car_tag WHERE tag_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        $spisak = $prep->fetchAll(PDO::FETCH_OBJ);
        $list = [];
        foreach ($spisak as $item) {
            $list[] = CarModel::getById($item->car_id);
        }
        return $list;
    }

    /**
     * Metod koji vrsi dodavanje zapisa taga u bazu podataka
     * @param string $name
     * @param string $classes
     * @return int|boolen ID broj zapisa u bazi ako je kreiran ili false ako je doslo do greske
     */
    public static function add($name, $classes) {
        $SQL = 'INSERT INTO tag (name, image_class) VALUES (?, ?);';
        $prep = DataBase::getInstance()->prepare($SQL);
        $res = $prep->execute([$name, $classes]);
        if ($res) {
            return DataBase::getInstance()->lastInsertId();
        } else {
            return false;
        }
    }

    /**
     * Metod koji vrsi izmenu zapisa taga u bazi podataka
     * @param int $id
     * @param string $name
     * @param string $classes
     * @return boolean
     */
    public static function edit($id, $name, $classes) {
        $SQL = 'UPDATE tag SET name = ?, image_class = ? WHERE tag_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        return $prep->execute([$name, $classes, $id]);
    }

}
