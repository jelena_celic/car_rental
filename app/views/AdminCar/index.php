<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article class="container">

    <section class="row">

        <!--Naslov stranice-->
        <header class="col-xs-12 col-md-12">         
            <h1 class="page-header" id="text-color"> Vozila
                <small>Spisak svih vozila</small>
            </h1>
        </header>
    </section>
    <table class="table table-hover table-condensed table-responsive">
        <thead>
            <tr>
                <td colspan="4" class="align-right">
                    <?php Misc::url('admin/cars/add/', 'Dodati novo vozilo'); ?>
                </td>
            </tr>
            <tr>
                <th>Id</th>
                <th>Vozilo</th>
                <th>Slug</th>
                <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($DATA['cars'] as $car): ?>
                <tr>
                    <td><?php echo $car->car_id; ?></td>
                    <td><?php echo htmlspecialchars($car->title); ?></td>
                    <td><?php echo htmlspecialchars($car->slug); ?></td>
                    <td>
                        <?php Misc::url('admin/cars/edit/' . $car->car_id, 'Izmena'); ?>
                        <?php Misc::url('admin/images/car/' . $car->car_id, 'Slika'); ?>

                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <!--Dugme za nazad-->
    <ul class="pager">
        <li><?php Misc::url('indexAdmin', 'Nazad'); ?></a></li>
    </ul>

</article>

