<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article  id="wrapper">
    <section class="row">
        
          <!--Naslov stranice-->
        <header class="col-sm-12 col-md-12">
            <h1 class="page-header" id="text-color">Dodavanje novog vozila</h1>
        </header>
    </section>
    <form method="post">

        <div class="form-group row">
            <div class="col-md-5">
                <label for="text" class="col-xs-2 col-form-label">Ime</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="title" class="form-control" id="title" required>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="slug" class="col-xs-2 col-form-label">Slug</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="slug" class="form-control" id="slug" required pattern="[a-z0-9\-]+">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="text" class="col-xs-2 col-form-label">Kratak tekst</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="short_text" class="form-control" id="short_text" required>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="long_text" class="col-xs-2 col-form-label">Detaljan opis</label>
                <div class="col-xs-10">
                    <textarea name="long_text" class="form-control" id="long_text" required></textarea>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="price" class="col-xs-2 col-form-label">Cena</label>
                <div class="col-xs-10">
                    <input class="form-control" type="number" name="price" class="form-control" id="price" required min="0.01" step="any">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="location_id" class="col-xs-2 col-form-label">Lokacija</label>
                <div class="col-xs-10">
                    <select name="location_id"  class="form-control" id="location_id">
                        <?php foreach ($DATA['locations'] as $item): ?>
                            <option value="<?php echo $item->location_id; ?>"><?php echo htmlspecialchars($item->name); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>

        <div class="form-group row">
            <div class="col-md-5">
                <label for="car_category_id" class="col-xs-2 col-form-label">Model</label>
                <div class="col-xs-10">
                    <select name="car_category_id" class="form-control"  id="car_category_id">
                        <?php foreach ($DATA['categories'] as $item): ?>
                            <option value="<?php echo $item->car_category_id; ?>"><?php echo htmlspecialchars($item->name); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">   
                <label for="tags_id" class="col-xs-2 col-form-label">Dodaci</label>

                <div class="col-xs-10">
                    <?php foreach ($DATA['tags'] as $tag): ?>
                        <input type="checkbox" name="tag_ids[]" value="<?php echo $tag->tag_id; ?>"> <?php echo htmlspecialchars($tag->name); ?>
                    <?php endforeach; ?>


                </div>

            </div>

        </div>
       
        <button type="submit" class="btn btn-default col-md-offset-1">Dodaj</button>   
        
    </form>
<!--Dugme za nazad-->
            <ul class="pager">
                <li><?php Misc::url('admin/cars', 'Nazad'); ?></a></li>
            </ul>
</article>

