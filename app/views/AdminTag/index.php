<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article class="container">

    <section class="row">

        <!--Naslov stranice-->
        <header class="col-xs-12 col-md-12">         
            <h1 class="page-header" id="text-color"> Dodatna oprema
                <small>Spisak sve dodatne opreme</small>
            </h1>
        </header>
    </section>

    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <td colspan="4" class="align-right">
                    <?php Misc::url('admin/tags/add/', 'Dodati novu opremu'); ?>
                </td>
            </tr>
            <tr>
                <th>Id</th>
                <th>Ime</th>
                <th>Klasa sličice</th>
                <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($DATA['tags'] as $tag): ?>
                <tr>
                    <td><?php echo $tag->tag_id; ?></td>
                    <td><?php echo htmlspecialchars($tag->name); ?></td>
                    <td><?php echo htmlspecialchars($tag->image_class); ?></td>
                    <td><?php Misc::url('admin/tags/edit/' . $tag->tag_id, 'Izmena'); ?></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
           <!--Dugme za nazad-->
    <ul class="pager">
        <li><?php Misc::url('indexAdmin', 'Nazad'); ?></a></li>
    </ul>
    
</article>


