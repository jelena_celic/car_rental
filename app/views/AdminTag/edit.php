<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article class="container">
    <section class="row">
        
          <!--Naslov stranice-->
        <header class="col-sm-12 col-md-12">
            <h1 class="page-header" id="text-color">Izmena dodatne opreme</h1>
        </header>
    </section>

    <form method="post">
        <div class="form-group row">
            <div class="col-md-5">
                <label for="name" class="col-xs-2 col-form-label">Ime opreme</label>
                <div class="col-xs-10">
                    <input type="text" name="name" class="form-control" id="name" required value="<?php echo htmlspecialchars($DATA['tag']->name); ?>">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="image_class" class="col-xs-2 col-form-label">Slika klase opreme</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="image_class" class="form-control" id="image_class" required pattern="[a-z0-9\- ]+" value="<?php echo htmlspecialchars($DATA['tag']->image_class); ?>">
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-default col-md-offset-1">Izmeni opremu</button>
    </form>
     <!--Dugme za nazad-->
            <ul class="pager">
                <li><?php Misc::url('admin/tags', 'Nazad'); ?></a></li>
            </ul>
</article>


