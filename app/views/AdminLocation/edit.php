<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<<article class="container">
    <section class="row">

        <!--Naslov stranice-->
        <header class="col-sm-12 col-md-12">
            <h1 class="page-header" id="text-color">Izmena lokacije</h1>
        </header>
    </section>

    <form method="post">
        <div class="form-group row">
            <div class="col-md-5">
                <label for="name" class="col-xs-2 col-form-label">Ime lokacije</label>
                <div class="col-xs-10">
                    <input class="form-control" ttype="text" name="name" class="form-control" id="name" required value="<?php echo htmlspecialchars($DATA['location']->name); ?>">
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="slug" class="col-xs-2 col-form-label">Slug lokacije</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="slug" class="form-control" id="slug" required pattern="[a-z0-9\-]+" value="<?php echo htmlspecialchars($DATA['location']->slug); ?>">
                </div>
            </div>
        </div> 
        <button type="submit" class="btn btn-default col-md-offset-1"> Izmeni lokaciju</button> 
    </form>
    <!--Dugme za nazad-->
    <ul class="pager">
        <li><?php Misc::url('admin/locations', 'Nazad'); ?></a></li>
    </ul>
</article>                     


