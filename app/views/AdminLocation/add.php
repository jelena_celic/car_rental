<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article class="container">
    <section class="row">
        
          <!--Naslov stranice-->
        <header class="col-sm-12 col-md-12">
            <h1 class="page-header" id="text-color">Dodavanje nove lokacije</h1>
        </header>
    </section>
    <form method="post">

        <div class="form-group row">
            <div class="col-md-5">
                <label for="name" class="col-xs-2 col-form-label">Ime lokacije</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="name" class="form-control" id="name" required>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-md-5">
                <label for="slug" class="col-xs-2 col-form-label">Slug lokacije</label>
                <div class="col-xs-10">
                    <input class="form-control" type="text" name="slug" class="form-control" id="slug" required pattern="[a-z0-9\-]+">
                </div>
            </div>
        </div>     
        <button type="submit" class="btn btn-default col-md-offset-1">Dodaj lokaciju</button>  
    </form>   
<!--Dugme za nazad-->
            <ul class="pager">
                <li><?php Misc::url('admin/locations', 'Nazad'); ?></a></li>
            </ul>
</article>                     




