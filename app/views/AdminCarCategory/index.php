<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article class="container">
    <section class="row">
        
        <!--Naslov stranice-->
        
        <header class="col-xs-12 col-md-12">
            <h1 class="page-header" id="text-color"> Kategorije
                <small>Spisak svih kategorija</small>
            </h1>          
        </header>
    </section>

        <table class="table table-hover table-condensed">
            <thead>
                <tr>
                    <td colspan="4" class="align-right">
                        <?php Misc::url('admin/categories/add/', 'Dodati novu kategoriju'); ?>
                    </td>
                </tr>
                <tr>
                    <th>Id</th>
                    <th>Ime</th>
                    <th>Slug</th>
                    <th>Opcije</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($DATA['categories'] as $category): ?>
                    <tr>
                        <td><?php echo $category->car_category_id; ?></td>
                        <td><?php echo htmlspecialchars($category->name); ?></td>
                        <td><?php echo htmlspecialchars($category->slug); ?></td>
                        <td><?php Misc::url('admin/categories/edit/' . $category->car_category_id, 'Izmeni'); ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       <!--Dugme za nazad-->
    <ul class="pager">
        <li><?php Misc::url('indexAdmin', 'Nazad'); ?></a></li>
    </ul>
    
        </article>

