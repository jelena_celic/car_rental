<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<article class="container">
    <section class="row">
        
        <!--Naslov stranice-->
        <header class="col-sm-12 col-md-12">
            <h1 class="page-header" id="text-color"> Slika
                <small>Spisak slika</small>
            </h1>
        </header>
    </section>
    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <td colspan="4" class="align-right">
                    <?php Misc::url('admin/images/car/' . $DATA['car_id'] . '/add', 'Dodati novu sliku ovom vozilu'); ?>
                </td>
            </tr>
            <tr>
                <th>Id</th>
                <th>Slika</th>
                <th>Opcije</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($DATA['images'] as $image): ?>
            <tr>
                <td><?php echo $image->image_id; ?></td>
                <td>
                    <img src="<?php echo Configuration::BASE . htmlspecialchars($image->path); ?>" class="small-image">
                </td>
                <td><?php Misc::url('admin/images//car/' . $DATA['car_id'] . '/edit/' . $image->image_id, 'Obrisati'); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
      <!--Dugme za nazad-->
        <ul class="pager">
            <li><?php Misc::url('admin/cars', 'Nazad'); ?></a></li>
        </ul>
</article>

