<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>

<<article class="container">
    <section class="row">
        <!--Naslov stranice-->
        <header class="col-sm-12 col-md-12">
            <h1 class="page-header" id="text-color">Dodavanje nove slike</font></h1>
        </header>
    </section>


    <form method="post" enctype="multipart/form-data">

        <div class="row">
            <div class="col-xs-12 col-md-4">
                <div class="form-group">
                    <label for="image">Izaberite sliku</label>

                    <input type="file" name="image" id="image" required><br>

                    <button type="submit" class="btn btn-default">Dodaj sliku</button>
                </div></div></div> 
    </form>

   
        <!--Dugme za nazad-->
        
        <ul class="pager">
            <li><?php Misc::url('admin/cars', 'Nazad'); ?></a></li>
        </ul>
</article>

