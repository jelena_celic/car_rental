<!--Stranica admin-->
<?php require_once 'app/views/_global/beforeContentAdmin.php'; ?>
<article class="container">



    <!-- Page content -->
    <div id="page-content-wrapper">
        <div class="content-header">

        </div>

        <div class="page-content inset" data-spy="" data-target="#spy">
            <div class="row">

                <div class="jumbotron text-center" >
                    <section class="row">
                        <div class="col-sm-12 col-md-12 text-center">
                            <div class="intro-message">
                                <h1> <span class="glyphicon glyphicon-user" aria-hidden="true"></span></h1> <h1  id="text-color"> Admin</h1>



                                <ul class="list-inline intro-social-buttons">
                                    <li >
                                        <div class="btn btn-default btn-lg btn-block"><span class="network-name"><?php Misc::url('admin/cars', 'Vozila'); ?></span></div>
                                    </li>
                                    <li>
                                        <div class="btn btn-default btn-lg btn-block"> <span class="network-name"><?php Misc::url('admin/categories', 'Kategorije'); ?></span></div>
                                    </li>
                                    <li>
                                        <div class="btn btn-default btn-lg btn-block"> <span class="network-name"><?php Misc::url('admin/locations', 'Lokacije'); ?></span></div>
                                    </li>
                                    <li>
                                        <div class="btn btn-default btn-lg btn-block"> <span class="network-name"><?php Misc::url('admin/tags', 'Dodatna oprema'); ?></span></div>
                                    </li>
                                </ul>                     
                            </div>
                        </div>
                    </section>
                </div>   
            </div>
        </div>
    </div>
</article>







