<!--Stranica o uslovima izajmljivanja--> 

<?php require_once 'app/views/_global/beforeContent.php'; ?>

<article class="blok">         
    <header><h2 class="text-center">Uslovi iznajmljivanja</h2></header>
    <br><br>  

    <section class="row">
        <div class="col-sm-12 col-md-4">

            <img class="img-circle img-responsive img-center" src="assets/img/children-not-allowed-signal.png" alt="">
            <h2>Osoba starija od 25 godina</h2>
            <p>Vozilo ne može iznajmiti osoba mlađa od 25 godina.<br>
                Iznajmljenim vozilom ne mogu upravljati lica koja nisu ovlašćena i upisana u ugovoru o najmu vozila 
                u suprotnom ukoliko dođe do eventualne štete na vozilu dok je upravljalo neovlašćeno lice, 
                korisnik snosi štetu 100%</p>
        </div>
        <div class="col-sm-12 col-md-4">

            <img class="img-circle img-responsive img-center" src="assets/img/id-card (2).png" alt="">
            <h2>Vozačka dozvola</h2>
            <p>Vozilo ne može iznajmiti osoba sa nevažećom vozačkom dozvolom i nevažećim ličnim dokumentima (lična karta ili pasoš).<br>
                Korišćenje vozila van granica Srbije nije dozvoljeno bez saglasnosti agencije.</p>
        </div>
        <div class="col-sm-12 col-md-4">

            <img class="img-circle img-responsive img-center" src="assets/img/euro (1).png" alt="">
            <h2>Depozit</h2>
            <p>Vozilo ne može iznajmiti osoba ukoliko nije dostavila depozit u iznosu od 200€.<br>
                Depozit se vraća prilikom predaje vozila.<br>
                U cenu svakog najma uključeno je kasko osiguranje kojim su sva naša vozila osigurana.</p>
        </div>
    </section>
</article>

<?php require_once 'app/views/_global/afterContent.php'; ?>