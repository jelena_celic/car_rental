<!--Stranica o iznajmljivanju-->

<?php require_once 'app/views/_global/beforeContent.php'; ?>


<br><br>

<article class="container">
    <section class="row ">        
        <div class="col-xs-12 col-md-6 col-md-offset-3">
            <?php if (isset($DATA['message'])): ?>
                <div class="alert">
                    <?php echo htmlspecialchars($DATA['message']); ?>
                </div>
            <?php endif; ?> 
            <!--Forma iznajmljivanja-->
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <legend class="text-center"id="contact-form">Iznajmljivanje vozila</legend><br>                
                    <div class="form-group">                        
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> Od:</span>
                                <input id="date_from" name="date_from" type="date" placeholder="Datum najama vozila" class="form-control">                              
                            </div>                
                        </div>    
                    </div>     
                    <div class="form-group">                        
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">                             
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> Do:</span>
                                <input id="date_to" name="date_to" type="date" placeholder="Datum povartka vozila" class="form-control">
                            </div>                
                        </div>    
                    </div>  
                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon">                   
                                    <i class="glyphicon glyphicon-road"></i> Vozilo: &nbsp;&nbsp;</span>
                                <select class="form-control" name="car_id">
                                    <option value="-1">- - -</option>
                                    <?php foreach ($DATA['cars'] as $car): ?>                                  
                                        <option value="<?php echo $car->car_id; ?>" <?php if ($car->car_id == @$DATA['car_id']) echo 'selected'; ?>>                                           
                                            <?php echo htmlspecialchars($car->title); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-map-marker"></i> Lokacija:</span>  
                                <select class="form-control" name="location_id">
                                    <option value="-1">- - -</option>
                                    <?php foreach ($DATA['locations'] as $location): ?>
                                        <option value="<?php echo $location->location_id; ?>" <?php if ($location->location_id == @$DATA['location_id']) echo 'selected'; ?>>
                                            <?php echo htmlspecialchars($location->name); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>                               
                        </div>
                    </div> 

                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i> Email:&nbsp;&nbsp;&nbsp;</span>
                                <input id="email" name="email" type="email" placeholder="Unesite email adresu" class="form-control" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i> Telefon:</span>
                                <input id="phone" name="phone" type="tel" placeholder="Unesite broj telefona" class="form-control" pattern="^[0-9]{4,}$" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-md-offset-1">    
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">Preko 25 godina
                        </label>                          
                    </div>

                    <br><br>
                    <div class="form-group">
                        <div class="col-md-12 text-center">        
                            <button type="submit" class="btn btn-default btn-lg">Iznajmi</button>
                        </div>
                    </div>
                </form> 
            </div>

        </div>
    </section>
</article>




<?php require_once 'app/views/_global/afterContent.php'; ?>
