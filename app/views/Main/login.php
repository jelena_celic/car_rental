<!--Stranica prijavljivanja-->
<?php require_once 'app/views/_global/beforeContent.php'; ?>
<br><br>
<article class="container">
    
    <header>
        <div class="col-xs-12 col-md-4 col-md-offset-4" >
            <div class="panel panel-default text-center">
                <div class="panel-heading">
                    <h3>Prijava na AutoRent</h3>
                </div>
            </div>
        </div>
    </header>
    <br>
    <!--Forma prijave-->  
    <form method="post">
        <div class="row text-center">
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="form-group">
                    <label for="f1_username" >Korisničko ime:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input type="text" name="username" class="form-control" id="f1_username" placeholder="Uniste korisničko ime" required class="input-field" pattern="^[a-z0-9]{4,}$">            
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-md-4 col-md-offset-4">
                <div class="form-group">
                    <label for="f1_password" >Lozinka:</label>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input type="password" name="password" class="form-control" id="f1_password" placeholder="Uniste lozinku"required class="input-field" pattern="^.{5,}$">          
                    </div>
                </div>   
            </div>

            <div class="col-xs-12 col-md-4 col-md-offset-4">                   
                <div class="checkbox">
                    <label><input type="checkbox"> Zapamti me</label>
                </div>                  
                <button type="submit" class="btn btn-default">Prijava</button>              
            </div>
        </div>
    </form>
 <br>
        <?php if (isset($DATA['message'])): ?>
            <div class="alert">
                <?php echo htmlspecialchars($DATA['message']); ?>
            </div>
        <?php endif; ?> 



</article>


<?php require_once 'app/views/_global/afterContent.php'; ?>