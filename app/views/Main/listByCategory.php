<?php require_once 'app/views/_global/beforeContent.php'; ?>

<article class="container">
    
    <!--Naslov stranice-->
    <section class="row">
        <header class="col-xs-12 col-md-12">
            <h1 class="page-header" >Vozila u kategoriji
                <small>&quot;<?php echo htmlspecialchars($DATA['category']->name); ?>&quot;</small>
            </h1> 
            <?php foreach ($DATA['cars'] as $car): ?>
                <?php require 'app/views/_global/car_item.php'; ?>
            <?php endforeach; ?> 
        </header>
        
        <!--Dugme za nazad-->
        
        <ul class="pager">
            <li><?php Misc::url('', 'Nazad'); ?></a></li>
        </ul>


    </section>

</article>



<?php require_once 'app/views/_global/afterContent.php'; ?>