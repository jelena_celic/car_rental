<!--Stranica cenovnik-->
<?php require_once 'app/views/_global/beforeContent.php'; ?>


<style>
  body {
      position: relative; 
  }
  
  #section1 {
      padding-top:50px;
      height:500px;
         
  
  }
  #section12 {padding-top:50px;
              height:500px;
              color: #fff; 
              background-color: #fff;
  }
  
  
  
  #section2 {padding-top:50px;height:500px;color: #fff; background-color: #673ab7;}
  #section3 {padding-top:50px;height:500px;color: #fff; background-color: #ff9800;}
  #section41 {padding-top:50px;height:500px;color: #fff; background-color: #00bcd4;}
  #section42 {padding-top:50px;height:500px;color: #fff; background-color: #009688;}
  </style>
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50">

<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">WebSiteName</a>
    </div>
    <div>
      <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav">
          <li><a href="#section1">Section 1</a></li>
          <li><a href="#section2">Section 2</a></li>
          <li><a href="#section3">Section 3</a></li>
          <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Section 4 <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="#section41">Section 4-1</a></li>
              <li><a href="#section42">Section 4-2</a></li>
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>    

<div id="section1" class="container-fluid">
    <header class="header-image">
    <div class="headline">
        <div class="container">
            <h1>AutoRent</h1>
            <h2>Dobrodošli</h2>
        </div>
    </div>
</header>
</div>
    
    <div id="section12" class="container-fluid">
        <form method="post" class="form" action="<?php echo Configuration::BASE; ?>search">  
         <!--Kategorije-->
        <div class="col-md-3">
            <p class="lead"id="p-category-color">Kategorije</p>
            <div class="list-group">
                <?php foreach ($DATA['categories'] as $category): ?>
                    <div class="list-group-item">  <?php Misc::url('category/' . $category->slug, $category->name); ?></div>
                <?php endforeach; ?>  
            </div>
        </div>
            <!--Forma pretrage-->
        <div class="col-sm-12 col-md-9">
            <div class="row carousel-holder">
                <div class="col-md-12 col-sm-12">
                    <header class="well text-center "  id="headline">
                        <h2>Potražite željeni auto u samo 3 koraka!</h2>
                        <div class="panel-body">
                            <div class="col-sm-12 col-md-8 col-md-offset-2">
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-map-marker"></i> Lokacija
                                        </span>
                                        <select class="form-control" name="location_id">
                                            <option value="-1">- - -</option>
                                            <?php foreach ($DATA['locations'] as $location): ?>
                                                <option value="<?php echo $location->location_id; ?>" <?php if ($location->location_id == @$DATA['location_id']) echo 'selected'; ?>>
                                                    <?php echo htmlspecialchars($location->name); ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div> 
                                <div class="form-group">                                
                                    <div class="input-group">                      
                                        <span class="input-group-addon">
                                            <i class="glyphicon glyphicon-list-alt"></i> Kategorija
                                        </span>  
                                        <select class="form-control" name="car_category_id">
                                            <option value="-1">- - -</option>
                                            <?php foreach ($DATA['categories'] as $category): ?>
                                                <option value="<?php echo $category->car_category_id; ?>" <?php if ($category->car_category_id == @$DATA['car_category_id']) echo 'selected'; ?>>
                                                    <?php echo htmlspecialchars($category->name); ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>  
                            </div>  
                            <div class="col-md-8 col-md-offset-2 col-sm-12">
                                <div class="form-group">
                                    <?php foreach ($DATA['tags'] as $tag): ?>
                                        <label class="checkbox-inline">
                                            <input type="checkbox" name="tag_ids[]" value="<?php echo $tag->tag_id; ?>" <?php if (@in_array($tag->tag_id, @$DATA['tag_ids'])) echo 'checked'; ?>>
                                            <i class="<?php echo htmlspecialchars($tag->image_class); ?>"></i>
                                            <?php echo htmlspecialchars($tag->name); ?>
                                        </label>
                                    <?php endforeach; ?>
                                </div>                     

                                <button type="submit" class="btn btn-default" id="btn-search">
                                    <i class="glyphicon glyphicon-search"></i>
                                    Pretraga
                                </button>                       
                            </div>
                        </div>                     
                    </header>
                </div>
            </div>
        </div>
          <hr>
        <!--Stranica Vozni park-->
        <div class="row">   
            <div id="headline2" class="col-md-3 col-sm-12">
                <h1>Izdavajamo:</h1>
                <h4> Na raspolaganju su Vam širok spektar putničkih vozila svih marka, modela i kategorija!</h4>
                <a class="btn btn-default btn-lg" href="<?php echo Configuration::BASE ?>carView/">Pregled svih vozila</a>
            </div>
            <!--Rezultat pretrage-->
            <div class="col-md-9 col-sm-12">
                <?php foreach ($DATA['cars'] as $car): ?>
                    <?php require 'app/views/_global/car_item.php'; ?>
                <?php endforeach; ?>
            </div>

        </div>    
    </form> 
    <br>
</div>
<div id="section2" class="container-fluid">
  <h1>Section 2</h1>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
</div>
<div id="section3" class="container-fluid">
  <h1>Section 3</h1>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
</div>
<div id="section41" class="container-fluid">
  <h1>Section 4 Submenu 1</h1>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
</div>
<div id="section42" class="container-fluid">
  <h1>Section 4 Submenu 2</h1>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
  <p>Try to scroll this section and look at the navigation bar while scrolling! Try to scroll this section and look at the navigation bar while scrolling!</p>
</div>

</body>
</html>


<?php require_once 'app/views/_global/afterContent.php'; ?>