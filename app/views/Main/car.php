<!--Stranica o vozilu-->
<?php require_once 'app/views/_global/beforeContent.php'; ?>

<article class="container">
    <section class="row">

        <!--Naslov stranice-->
        <header class="col-md-12">
            <h1 class="page-header" > <?php echo htmlspecialchars($DATA['car']->title); ?></h1>                                 
        </header>
    </section>

    <!--Detalji vozila-->

    <section class="row">
        <section class="col-md-8">
            <?php foreach ($DATA['car']->images as $image): ?>
                <img src="<?php echo Configuration::BASE . $image->path; ?>" alt="<?php echo htmlspecialchars($DATA['car']->title) . ' - ' . $image->image_id; ?>" class="gallery-image img-responsive center-block">
            <?php endforeach; ?>
        </section>
       
        <section class="col-md-4">
            <h3>  Specifikacija</h3>               
            <p><?php echo $DATA['car']->long_text; ?></p>
        </section>
    </section>
    <hr>
    <div class="row"> 
        <div class="col-md-3 col-sm-12 ">
            <h3> Dodatna oprema</h3>
            <ul>    <?php foreach ($DATA['car']->tags as $tag): ?>
                    <li>     <?php echo htmlspecialchars($tag->name); ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-3 col-sm-12 "> 
            <h3> Dodatne informacije</h3>

            <p>   <i class="glyphicon glyphicon-map-marker"></i> <?php echo htmlspecialchars($DATA['car']->location->name); ?>    <p> 
            <p>   <i class="glyphicon glyphicon-th-list"></i> <?php echo htmlspecialchars($DATA['car']->car_category->name); ?></p>
        </div>
        <div class="col-md-3 col-sm-12">
            <h3>Cena po danu:</h3>
            <h4><span class="label label-default "><?php echo $DATA['car']->price; ?> &euro;</span></h4>

        </div>
        <div class="col-md-3 col-sm-12">
            <br>    
            <a class="btn btn-lg btn-default btn-block" href="<?php echo Configuration::BASE ?>carRent/">Iznajmi!</a>                
        </div>
    </div>

    <!--Dugme za nazad-->
    <ul class="pager">
        <li><?php Misc::url('', 'Nazad'); ?></a></li>
    </ul>

</article>


<?php require_once 'app/views/_global/afterContent.php'; ?>