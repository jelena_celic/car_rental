<!--Kontakt stranica-->
<?php require_once 'app/views/_global/beforeContent.php'; ?>

<article class="blok">  

    <section class="row">
        <div class="col-xs-12 col-md-12">
            <?php if (isset($DATA['message'])): ?>
                 <div class="alert">
                <?php echo htmlspecialchars($DATA['message']); ?>
                 </div>
            <?php endif; ?> 
            
        </div>
        <!--Forma kontakta-->
        <div class="col-md-6">
            <div class="well well-sm">
                <form  id="contact_form" class="form-horizontal" method="post" >


                    <legend class="text-center" id="contact-form">Kontaktirajte nas</legend><br> 

                    <div class="form-group">                                                                              
                        <div class="col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i> Ime: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                <input id="first_name" name="first_name" type="text" placeholder="Unesite ime" class="form-control" pattern="^[A-Za-z]{1,32}$" required />
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i> Prezime:</span>
                                <input id="last_name" name="last_name" type="text" placeholder="Unesite prezime" class="form-control" pattern="^[A-Za-z]{1,32}$" required />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i> Email: &nbsp;&nbsp;&nbsp;</span>
                                <input id="email" name="email" type="email" placeholder="Unesite email adresu" class="form-control" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required />
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i> Telefon:&nbsp;</span>
                                <input id="phone" name="phone" type="tel" placeholder="Unesite broj telefona" class="form-control" pattern="^[0-9]{4,}$" required  />
                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-1">
                            <textarea class="form-control" id="message" name="message" placeholder="Ostavite poruku" rows="7" required ></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-12 text-center"><br>                                                        
                            <button type="submit" class="btn btn-default btn-lg">Pošalji</button>                          
                        </div>
                    </div>
                </form>        
            </div>

        </div>
        <div class="col-md-6">
            <div>
                <div class="panel panel-default">
                    <div class="text-center contact">Lokacija</div>
                    <div class="panel-body text-center">
                        <h4>Adresa</h4>
                        <div>
                            Kralja Petra 1<br>
                            11 000 Beograd<br>
                            +(381) 11 22 1234<br>
                            vozila@rental.rs<br>
                        </div>
                        <hr>                                             
                        <div id="googleMap" class="embed-responsive embed-responsive-16by9" >
                            <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10582.893153880575!2d20.451048390744745!3d44.81827645842664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a654b1ecbfe71%3A0xc196f5c9fffcfd51!2z0JrRgNCw0ZnQsCDQn9C10YLRgNCwLCDQkdC10L7Qs9GA0LDQtA!5e0!3m2!1ssr!2srs!4v1465503144831"
                                    width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>                   
                    </div>
                </div>
            </div>
        </div>
    </section>  
</article>

<?php require_once 'app/views/_global/afterContent.php'; ?>

