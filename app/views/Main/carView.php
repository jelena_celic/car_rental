<!--Stranica o detaljima vozila-->
<?php require_once 'app/views/_global/beforeContent.php'; ?>

<article class="container">
    <!-- Naslov strane -->

    <section class="row">
        <div class="col-sm- col-md-12">
            <h1 class="page-header" id="text-color"> Vozni park
                <small>Sva vozila</small>
            </h1>
        </div>
    </section>
    <!-- Prikaz vozila -->
    <?php foreach ($DATA['cars'] as $car): ?>
        <section class="col-md-4 col-sm-6 hero-feature">
            <div class="thumbnail text-center">
                <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>">
                    <img src="<?php echo Configuration::BASE . $car->images[0]->path; ?>" alt="<?php echo htmlspecialchars($car->title); ?>" class="car-image">
                </a>
                <div class="caption">
                    <h3 id="text-color">
                        <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>">
                            <?php echo htmlspecialchars($car->title); ?>
                        </a>
                    </h3>
                    <h4><span class="label label-default"><?php echo htmlspecialchars($car->price); ?> &euro;</span></h4>  
                    <p>
                        <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>" class="btn btn-default">
                            Iznajmi
                        </a>
                    </p>

                </div>
            </div>
            
        </section>
    
    <?php endforeach; ?> 
    <div class="col-md-12">
  <!--Dugme za nazad-->
    <ul class="pager">
        <li><?php Misc::url('', 'Nazad'); ?></a></li>
    </ul>
  </div>
</article>
<?php require_once 'app/views/_global/afterContent.php'; ?>