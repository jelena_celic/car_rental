<?php require_once 'app/views/_global/beforeContent.php'; ?>


    <div id="home-page" class="container-fluid">
        <header class="header-image">
            <div class="headline">
                <div class="container">
                    <h1>AutoRent</h1>
                    <h2>Dobrodošli</h2>
                </div>
            </div>
        </header>
    </div>

    <div id="home-page-two" class="container-fluid">
        <div class="container">
            <form method="post" class="form" action="<?php echo Configuration::BASE; ?>search">  
                <!--Kategorije-->
                <div class="col-md-3">
                    <p class="lead"id="p-category-color">Kategorije</p>
                    <div class="list-group">
                        <?php foreach ($DATA['categories'] as $category): ?>
                            <div class="list-group-item">  <?php Misc::url('category/' . $category->slug, $category->name); ?></div>
                        <?php endforeach; ?>  
                    </div>
                </div>
                <!--Forma pretrage-->
                <div class="col-sm-12 col-md-9">
                    <div class="row carousel-holder">
                        <div class="col-md-12 col-sm-12">
                            <header class="well text-center "  id="headline">
                                <h2>Potražite željeni auto u samo 3 koraka!</h2>
                                <div class="panel-body">
                                    <div class="col-sm-12 col-md-8 col-md-offset-2">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-map-marker"></i> Lokacija
                                                </span>
                                                <select class="form-control" name="location_id">
                                                    <option value="-1">- - -</option>
                                                    <?php foreach ($DATA['locations'] as $location): ?>
                                                        <option value="<?php echo $location->location_id; ?>" <?php if ($location->location_id == @$DATA['location_id']) echo 'selected'; ?>>
                                                            <?php echo htmlspecialchars($location->name); ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div> 
                                        <div class="form-group">                                
                                            <div class="input-group">                      
                                                <span class="input-group-addon">
                                                    <i class="glyphicon glyphicon-th-list"></i> Kategorija
                                                </span>  
                                                <select class="form-control" name="car_category_id">
                                                    <option value="-1">- - -</option>
                                                    <?php foreach ($DATA['categories'] as $category): ?>
                                                        <option value="<?php echo $category->car_category_id; ?>" <?php if ($category->car_category_id == @$DATA['car_category_id']) echo 'selected'; ?>>
                                                            <?php echo htmlspecialchars($category->name); ?>
                                                        </option>
                                                    <?php endforeach; ?>
                                                </select>
                                            </div>
                                        </div>  
                                    </div>  
                                    <div class="col-md-8 col-md-offset-2 col-sm-12">
                                        <div class="form-group">
                                            <?php foreach ($DATA['tags'] as $tag): ?>
                                                <label class="checkbox-inline">
                                                    <input type="checkbox" name="tag_ids[]" value="<?php echo $tag->tag_id; ?>" <?php if (@in_array($tag->tag_id, @$DATA['tag_ids'])) echo 'checked'; ?>>
                                                    <i class="<?php echo htmlspecialchars($tag->image_class); ?>"></i>
                                                    <?php echo htmlspecialchars($tag->name); ?>
                                                </label>
                                            <?php endforeach; ?>
                                        </div>                     

                                        <button type="submit" class="btn btn-default" id="btn-search">
                                            <i class="glyphicon glyphicon-search"></i>
                                            Pretraga
                                        </button>                       
                                    </div>
                                </div>                     
                            </header>
                        </div>
                    </div>
                </div>

                <!--Stranica Vozni park-->
                <div class="row">   
                    <div id="headline2" class="col-md-3 col-sm-12">
                        <h1>Izdavajamo:</h1>
                        <h4> Na raspolaganju su Vam širok spektar putničkih vozila svih marka, modela i kategorija!</h4>
                        <a class="btn btn-default btn-lg" href="<?php echo Configuration::BASE ?>carView/">Pregled svih vozila</a>
                    </div>
                    <!--Rezultat pretrage-->
                    <div class="col-md-9 col-sm-12">
                        <?php foreach ($DATA['cars'] as $car): ?>
                            <?php require 'app/views/_global/car_item.php'; ?>
                        <?php endforeach; ?>
                    </div>

                </div>    
            </form> 
        </div>
    </div>
    <div id="about-us" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <font color="#761c19">   O AutoRent-u</font>
                    </h1>
                </div>
            </div>
            <div class="row text-center" >
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" >
                            <h4><i class="glyphicon glyphicon-time"></i> Pružamo 00h-24h</h4>
                        </div>                       
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" >
                            <h3><i class="glyphicon glyphicon-info-sign"></i> Nudimo putno osiguranje</h3>
                        </div>                       
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading" >
                            <h2><i class="glyphicon glyphicon-gift"></i> Dajemo gartis GPS navigaciju</h2>
                        </div>                    
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">   
                <div id="headline2" class="col-md-3 col-sm-12 text-center">
                    <h2>Uslovi:</h2>
                    <h4> Za iznajmljivanje putničkih vozila neophodno je da korisnik ispuniti sledeće uslove</h4>
                    <a class="btn btn-default btn-lg" ><i class="glyphicon glyphicon-arrow-right"></i></a>      
                    <a class="btn btn-default btn-lg" ><i class="glyphicon glyphicon-arrow-right"></i></a> 
                    <a class="btn btn-default btn-lg" ><i class="glyphicon glyphicon-arrow-right"></i></a> 
                    
                </div>

                <div class="col-md-3 col-sm-12 ">

                    <img class="img-circle img-responsive img-center" src="assets/img/u1.png" alt="">
                    <font color="#761c19">     <h3>Punoletna osoba</h3></font>

                </div>
                <div class="col-md-3 col-sm-12 ">

                    <img class="img-circle img-responsive img-center" src="assets/img/a2.png" alt="">
                    <font color="#761c19">    <h3>Depozit</h3></font>

                </div>
                <div class="col-md-3 col-sm-12">

                    <img class="img-circle img-responsive img-center" src="assets/img/u3.png" alt="">
                    <font color="#761c19">     <h3>Vozačka dozvola</h3></font>

                </div>
            </div>
        </div>
    </div>
    <div id="cars" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <font color="#761c19"> Vozni park</font>
                    </h1>
                </div>
                <?php foreach ($DATA['cars'] as $car): ?>
                    <div class="col-md-4 col-sm-6 hero-feature">
                        <div class="thumbnail text-center">
                            <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>">
                                <img src="<?php echo Configuration::BASE . $car->images[0]->path; ?>" alt="<?php echo htmlspecialchars($car->title); ?>" class="car-image">
                            </a>
                            <div class="caption">
                                <h3>
                                    <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>">
                                        <?php echo htmlspecialchars($car->title); ?>
                                    </a>
                                </h3>
                                <p><?php echo htmlspecialchars($car->short_text); ?></p>
                                <p>
                                    <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>" class="btn btn-default">
                                        Više...
                                    </a>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?> 

            </div>


            <hr>
            <div class="well">
                <div class="row">
                    <div class="col-md-8">
                        <h4>Ukoliko ispunjavate sve uslove iznajmljivanja, na raspolaganju su Vam širok asortiman putničkih vozila agencije AutoRent.</h4>
                    </div>                            
                    <div class="col-md-4">
                        
                        <button type="button" class="btn btn-lg btn-default btn-block" data-toggle="modal" data-target="#myModal">Iznajmi</button>  
                        
                    </div>
                    <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">AutoRent</h4>
          <?php if (isset($DATA['message'])): ?>
                <div class="alert">
                    <?php echo htmlspecialchars($DATA['message']); ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="modal-body">
           <section class="row ">        
        <div class="col-xs-12 col-md-6 col-md-offset-3">
             
            <!--Forma iznajmljivanja-->
            <div class="well well-sm">
                <form class="form-horizontal" method="post">
                    <legend class="text-center"id="contact-form">Iznajmljivanje vozila</legend><br>                
                    <div class="form-group">                        
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> Od:</span>
                                <input id="date_from" name="date_from" type="date" placeholder="Datum najama vozila" class="form-control">                              
                            </div>                
                        </div>    
                    </div>     
           <div class="form-group">                        
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">                             
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i> Do:</span>
                                <input id="date_to" name="date_to" type="date" placeholder="Datum povartka vozila" class="form-control">
                            </div>                
                        </div>    
                    </div>  
                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon">                   
                                    <i class="glyphicon glyphicon-road"></i> Vozilo: &nbsp;&nbsp;</span>
                                <select class="form-control" name="car_id">
                                    <option value="-1">- - -</option>
                                    <?php foreach ($DATA['cars'] as $car): ?>                                  
                                        <option value="<?php echo $car->car_id; ?>" <?php if ($car->car_id == @$DATA['car_id']) echo 'selected'; ?>>                                           
                                            <?php echo htmlspecialchars($car->title); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="glyphicon glyphicon-map-marker"></i> Lokacija:</span>  
                                <select class="form-control" name="location_id">
                                    <option value="-1">- - -</option>
                                    <?php foreach ($DATA['locations'] as $location): ?>
                                        <option value="<?php echo $location->location_id; ?>" <?php if ($location->location_id == @$DATA['location_id']) echo 'selected'; ?>>
                                            <?php echo htmlspecialchars($location->name); ?>
                                        </option>
                                    <?php endforeach; ?>
                                </select>
                            </div>                               
                        </div>
                    </div> 

                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i> Email:&nbsp;&nbsp;&nbsp;</span>
                                <input id="email" name="email" type="email" placeholder="Unesite email adresu" class="form-control" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12 col-md-10 col-md-offset-1">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i> Telefon:</span>
                                <input id="phone" name="phone" type="tel" placeholder="Unesite broj telefona" class="form-control" pattern="^[0-9]{4,}$" required>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6 col-md-offset-1">    
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">Preko 25 godina
                        </label>                          
                    </div>

                    <br><br>
                    <div class="form-group">
                        <div class="col-md-12 text-center">        
                            <button type="submit" class="btn btn-default btn-lg">Iznajmi</button>
                        </div>
                    </div>
                </form> 
            </div>

        </div>
    </section> 

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>
        </div>
      </div>
    </div>
  </div>
                    
                    
                </div>
            </div>

        </div>
    </div>
    <div id="contact" class="container-fluid">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">
                        <font color="#fff"> Kontakt</font>
                    </h1>
                </div>
                <!--Forma kontakta-->
                <div class="col-md-6">
                    <div class="well well-sm">
                        <form  id="contact_form" class="form-horizontal" method="post" >


                            <legend class="text-center" id="contact-form">Kontaktirajte nas</legend><br> 

                            <div class="form-group">                                                                              
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i> Ime: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                        <input id="first_name" name="first_name" type="text" placeholder="Unesite ime" class="form-control" pattern="^[A-Za-z]{1,32}$" required />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i> Prezime:</span>
                                        <input id="last_name" name="last_name" type="text" placeholder="Unesite prezime" class="form-control" pattern="^[A-Za-z]{1,32}$" required />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i> Email: &nbsp;&nbsp;&nbsp;</span>
                                        <input id="email" name="email" type="email" placeholder="Unesite email adresu" class="form-control" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-phone-alt"></i> Telefon:&nbsp;</span>
                                        <input id="phone" name="phone" type="tel" placeholder="Unesite broj telefona" class="form-control" pattern="^[0-9]{4,}$" required  />
                                    </div>
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-1">
                                    <textarea class="form-control" id="message" name="message" placeholder="Ostavite poruku" rows="7" required ></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12 text-center"><br>                                                        
                                    <button type="submit" class="btn btn-default btn-lg">Pošalji</button>                          
                                </div>
                            </div>
                        </form>        
                    </div>

                </div>
                <div class="col-md-6">
                    <div>
                        <div class="panel panel-default">
                            <div class="text-center contact">Lokacija</div>
                            <div class="panel-body text-center">
                                <h4>Adresa</h4>
                                <div>
                                    Kralja Petra 1<br>
                                    11 000 Beograd<br>
                                    +(381) 11 22 1234<br>
                                    vozila@rental.rs<br>
                                </div>
                                <hr>                                             
                                <div id="googleMap" class="embed-responsive embed-responsive-16by9" >
                                    <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d10582.893153880575!2d20.451048390744745!3d44.81827645842664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x475a654b1ecbfe71%3A0xc196f5c9fffcfd51!2z0JrRgNCw0ZnQsCDQn9C10YLRgNCwLCDQkdC10L7Qs9GA0LDQtA!5e0!3m2!1ssr!2srs!4v1465503144831"
                                            width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>                   
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>




<?php require_once 'app/views/_global/afterContent.php'; ?>




