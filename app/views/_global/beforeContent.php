<!doctype html>
<html>
    <head>
        <title><?php echo @$DATA['seo_title']; ?></title>
        <meta charset="utf-8">
        <link href="<?php echo Configuration::BASE; ?>assets/css/one-page-wonder.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/main.css" rel="stylesheet">       
        <link href="<?php echo Configuration::BASE; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/mobile.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>-mobile.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/slider.css" rel="stylesheet">
         <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body data-spy="scroll" data-target=".navbar" data-offset="50">




    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>                        
                </button>
                <a class="navbar-brand" href="#">AutoRent</a>
            </div>
            <div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">                       
                        <li><?php Misc::url('#home-page', 'Početna'); ?></li>
                        <li><?php Misc::url('#about-us', 'O nama'); ?></li>
                        <li><?php Misc::url('#cars', 'Vozni park'); ?></li>
                        <li><?php Misc::url('#contact', 'Kontakt'); ?> </li>
                        <li><?php Misc::url('login', 'Prijava'); ?>  </li> 
                    </ul>
                </div>
            </div>
        </div>
    </nav>    

        <main class="sadrzaj">
 <br> <br> 