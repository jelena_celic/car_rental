<!doctype html>
<html>
    <head>
        <title><?php echo @$DATA['seo_title']; ?></title>
        <meta charset="utf-8">

        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/main.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/mobile.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>-mobile.css" rel="stylesheet">



    </head>
    <body>

