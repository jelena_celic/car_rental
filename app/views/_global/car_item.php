<!--Forma stranice o detaljima vozila-->

<div class="col-sm-12 col-md-4"> 
    <div class="thumbnail">
        <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>">
            <img src="<?php echo Configuration::BASE . $car->images[0]->path; ?>" alt="<?php echo htmlspecialchars($car->title); ?>" class="car-image">
        </a>
        <div class="caption">
            <h3>
                <a href="<?php echo Configuration::BASE ?>car/<?php echo $car->slug; ?>">
                    <?php echo htmlspecialchars($car->title); ?>
                </a>
            </h3>
            <p> <?php echo htmlspecialchars($car->short_text); ?></p>
           
        </div>       
    </div>
</div>





