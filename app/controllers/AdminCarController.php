<?php
   /**
 * Klasa kontrolera admin panela aplikacije za rad sa vozilima
 */
class AdminCarController extends AdminController{
  /**
     * Indeks metod admin kontrolera za rad sa putnickim vozilima prikazuje spisak svih putnickih vozila
     */
   public function index() {
        $this->set('cars', CarModel::getAll());
    }

    /**
     * Ovaj metod prikazuje formuilar za dodavanje ili vrsi dodavanje ako su podaci poslati HTTP POST metodom
     * @return void
     */
    public function add() {
        $this->set('locations', LocationModel::getAll());
        $this->set('categories', CarCategoryModel::getAll());
        $this->set('tags', TagModel::getAll());

        if (!$_POST) return;

        $title = filter_input(INPUT_POST, 'title');
        $slug = filter_input(INPUT_POST, 'slug');
        $short_text = filter_input(INPUT_POST, 'short_text');
        $long_text = filter_input(INPUT_POST, 'long_text');
        $price = floatval(filter_input(INPUT_POST, 'price'));

        $location_id = filter_input(INPUT_POST, 'location_id', FILTER_SANITIZE_NUMBER_INT);
        $car_category_id = filter_input(INPUT_POST, 'car_category_id', FILTER_SANITIZE_NUMBER_INT);
        
        $car_id = CarModel::add($title, $slug, $short_text, $long_text, $price, $location_id, $car_category_id, Session::get('user_id'));

        if ($car_id) {
            $tag_ids = filter_input(INPUT_POST, 'tag_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

            foreach ($tag_ids as $tag_id) {
                CarModel::addTagToCar($car_id, $tag_id);
            }

            Misc::redirect('admin/cars/');
        } else {
            $this->set('message', 'Doslo je do greske prilikom dodavanja putničkog vozila u bazu podataka.');
        }
    }

    /**
     * Ovaj metod prikazuje formuilar za izmenu ili vrsi izmenu ako su podaci poslati HTTP POST metodom
     * @return void
     */
    public function edit($id) {
        $this->set('locations', LocationModel::getAll());
        $this->set('categories', CarCategoryModel::getAll());
        $this->set('tags', TagModel::getAll());

        $car = CarModel::getById($id);

        if (!$car) {
            Misc::redirect('admin/cars/');
        }

        $car->tags = CarModel::getTagsForCarId($id);
        $car->tag_ids = [];
        foreach ($car->tags as $tag) {
            $car->tag_ids[] = $tag->tag_id;
        }
        $this->set('car', $car);

        if (!$_POST) return;

        $title = filter_input(INPUT_POST, 'title');
        $slug = filter_input(INPUT_POST, 'slug');
        $short_text = filter_input(INPUT_POST, 'short_text');
        $long_text = filter_input(INPUT_POST, 'long_text');
        $price = floatval(filter_input(INPUT_POST, 'price'));

        $location_id = filter_input(INPUT_POST, 'location_id', FILTER_SANITIZE_NUMBER_INT);
        $car_category_id = filter_input(INPUT_POST, 'car_category_id', FILTER_SANITIZE_NUMBER_INT);

        $res = CarModel::edit($id, $title, $slug, $short_text, $long_text, $price, $location_id, $car_category_id);

        $tag_ids = filter_input(INPUT_POST, 'tag_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

        CarModel::delateAllTags($id);
        foreach ($tag_ids as $tag_id) {
            CarModel::addTagToCar($id, $tag_id);
        }

        if ($res) {
            Misc::redirect('admin/cars/');
        } else {
            $this->set('message', 'Doslo je do greske prilikom izmene podataka o putničkom vozilu.');
        }
    }
}
