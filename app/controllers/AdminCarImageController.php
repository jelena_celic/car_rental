<?php

/**
 * Klasa kontrolera admin panela prikazuje rad sa slikama putnickih vozila
 */
class AdminCarImageController extends AdminController {

    /**
     * Metod stranice koja lista sve slike odabranog putnickog vozila
     * @param int car_id
     */
    public function index() {
        
    }

    public function listCarImages($car_id) {
        $this->set('images', ImageModel::getByCarId($car_id));
        $this->set('car_id', $car_id);
    }

    /**
     * Metod koji vrsi dodavanej slike putnickog vozila
     * @param int $car_id
     */
    public function uploadImage($car_id) {
        $this->set('car_id', $car_id);

        if (!$_FILES or ! isset($_FILES['image']))
            return;

        if ($_FILES['image']['error'] != 0) {
            $this->set('message', 'Doslo je do greske prilikom dodavanja fajla!');
            return;
        }

        $temporaryPath = $_FILES['image']['tmp_name'];
        $fileSize = $_FILES['image']['size'];
        $originalName = $_FILES['image']['name'];

        if ($fileSize > 300 * 1024) {
            $this->set('message', 'Fajl koji dodajete je veci od maksimalne dozvojene velicine od 300KB!');
            return;
        }

        $finfo = new finfo(FILEINFO_MIME_TYPE);
        $mimeType = $finfo->file($temporaryPath);

        if ($mimeType != 'image/jpeg') {
            $this->set('message', 'Dozvojeno je dodavanje samo JPG slika!');
            return;
        }

        $baseName = basename($originalName);
        $baseName = strtolower($baseName);
        $baseName = preg_replace('[^a-z0-9\- ]', '', $baseName);
        $baseName = preg_replace(' +', '-', $baseName);

        $fileName = date('YmdHisu') . '-' . $baseName . '.jpg';

        $newLocation = Configuration::IMAGE_DATA_PATH . $fileName;

        $res = move_uploaded_file($temporaryPath, $newLocation);
        if (!$res) {
            $this->set('message', 'Doslo je do greske prilikom cuvanja fajla na krajnju lokaciju. Nemate privilegije za upis u ovaj direktorijum!');
            return;
        }

        $res = ImageModel::add($newLocation, $car_id);
        if (!$res) {
            $this->set('message', 'Doslo je do greske prilikom upisa u bazu podataka!');
            return;
        }

        Misc::redirect('admin/images/car/' . $car_id);
    }

}
