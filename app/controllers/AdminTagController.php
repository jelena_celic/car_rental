<?php
/**
 * Klasa kontrolera admin panela aplikacije za rad sa tagovima
 */
class AdminTagController extends AdminController {
    /**
     * Indeks metod admin kontrolera za rad sa tagovima prikazuje spisak svih lokacija
     */
    public function index() {
        $this->set('tags', TagModel::getAll());
    }

    /**
     * Ovaj metod prikazuje formuilar za dodavanje ili vrsi dodavanje ako su podaci poslati HTTP POST metodom
     * @return void
     */
    public function add() {
        if (!$_POST) return;

        $name = filter_input(INPUT_POST, 'name');
        $image_class = filter_input(INPUT_POST, 'image_class');

        $tag_id = TagModel::add($name, $image_class);

        if ($tag_id) {
            Misc::redirect('admin/tags/');
        } else {
            $this->set('message', 'Doslo je do greske prilikom dodavanja taga u bazu podataka.');
        }
    }

    /**
     * Ovaj metod prikazuje formuilar za izmenu ili vrsi izmenu ako su podaci poslati HTTP POST metodom
     * @return void
     */
    public function edit($id) {
        $tag = TagModel::getById($id);

        if (!$tag) {
            Misc::redirect('admin/tags/');
        }

        $this->set('tag', $tag);

        if (!$_POST) return;

        $name = filter_input(INPUT_POST, 'name');
        $image_class = filter_input(INPUT_POST, 'image_class');

        $res = TagModel::edit($id, $name, $image_class);

        if ($res) {
            Misc::redirect('admin/tags/');
        } else {
            $this->set('message', 'Doslo je do greske prilikom izmene podataka o tagu.');
        }
    }
}
