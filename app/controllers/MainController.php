<?php

/**
 * Ovo je osnovni kontroler aplikacije koji se koristi za izvrsavanje
 * zahteva upucenih prema podrazumevanim rutama koje poznaje veb sajt.
 */
class MainController extends Controller {

    /**
     * Osnovni metod pocetne stranice sajta
     */
    function index($page = 0) {
        $this->set('categories', CarCategoryModel::getAll());
        $this->set('locations', LocationModel::getAll());
        $this->set('tags', TagModel::getAll());

        $cars = CarModel::getAllPaged($page);
        for ($i = 0; $i < count($cars); $i++) {
            $cars[$i]->images = CarModel::getCarImages($cars[$i]->car_id);
            $cars[$i]->tags = CarModel::getTagsForCarId($cars[$i]->car_id);
        }
        $this->set('cars', $cars);
    }

    /**
     * Metod kontakt stranice sajta
     */
    function contact() {
        $this->set('contacts', ContactModel::getAll());

        if ($_POST) {
            $contact_id = filter_input(INPUT_POST, 'contact_id', FILTER_SANITIZE_NUMBER_INT);
            $first_name = filter_input(INPUT_POST, 'first_name');
            $last_name = filter_input(INPUT_POST, 'last_name');
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $phone = filter_input(INPUT_POST, 'phone');
            $message = filter_input(INPUT_POST, 'message');

            $contact_sent = ContactModel::add($contact_id, $first_name, $last_name, $email, $phone, $message);

            if ($contact_sent) {
                $this->set('message', 'Kontakt je uspesno poslat.');
            } else {
                $this->set('message', 'Doslo je do neke greske. Pozovite administratora.');
            }
        }
    }

    /**
     * Metod stranice cenovnik sajta
     */
    function pricelist() {
        $this->set('pricelists', PricelistModel::getAll());
   
     
    }

    /**
     * Metod stranice iznajmljivanja sajta
     */
    function aboutRent() {
        
    }

    /**
     * Metod admin stranice  sajta
     */
    function indexAdmin() {
        
    }

    function carView() {
        $this->set('cars', CarModel::getAll());
        $this->set('categories', CarCategoryModel::getAll());
        $this->set('locations', LocationModel::getAll());
        $this->set('tags', TagModel::getAll());

        $cars = CarModel::getAll();
        for ($i = 0; $i < count($cars); $i++) {
            $cars[$i]->images = CarModel::getCarImages($cars[$i]->car_id);
            $cars[$i]->tags = CarModel::getTagsForCarId($cars[$i]->car_id);
        }
        $this->set('cars', $cars);
    }

    function carRent() {
        $this->set('cars', CarModel::getAll());
        $this->set('locations', LocationModel::getAll());

        if ($_POST) {
            $date_from = filter_input(INPUT_POST, 'date_from');
            $date_to = filter_input(INPUT_POST, 'date_to');
            $car_id = filter_input(INPUT_POST, 'car_id', FILTER_SANITIZE_NUMBER_INT);
            $location_id = filter_input(INPUT_POST, 'location_id', FILTER_SANITIZE_NUMBER_INT);
            $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
            $phone = filter_input(INPUT_POST, 'phone');

            $car_rent_id = CarRentModel::add($car_id, $location_id, $date_from, $date_to, $phone, $email);

            if ($car_rent_id) {
                $this->set('message', 'Vozilo je uspesno iznajmljeno.');
            } else {
                $this->set('message', 'Doslo je do neke greske. Pozovite administratora.');
            }
        }
    }

    /**
     * Ovaj metod proverava da li postoje podaci za prijavu poslati HTTP POST
     * metodom, vrsi njihovu validaciju, proverava postojanje korisnika sa tim
     * pristupnim parametrima i u slucaju da sve provere prodju bez greske
     * metod kreira sesiju za korisnika i preusemrava korisnika na default rutu.
     * @return void Metod ne vraca nista, vec koristi return naredbu za prekid izvrsavanja u odredjenim situacijama
     */
    function login() {
        if ($_POST) {
            $username = filter_input(INPUT_POST, 'username');
            $password = filter_input(INPUT_POST, 'password');

            if (!preg_match('/^[a-z0-9]{4,}$/', $username) or ! preg_match('/^.{5,}$/', $password)) {
                $this->set('message', 'Pogrešno uneto korisničko ime ili lozinka.');
                return;
            }

            $passwordHash = hash('sha512', $password . Configuration::SALT);
            $user = UserModel::getActiveUserByUsernameAndPasswordHash($username, $passwordHash);

            if (!$user) {
                $this->set('message', 'Korisničko ime ili loznika nisu validni ili korisnik nije aktivan.');
                return;
            }

            Session::set('user_id', $user->user_id);
            Session::set('username', $username);
            Session::set('user_ip', filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_FLAG_IPV4));
            Session::set('user_agent', filter_input(INPUT_SERVER, 'HTTp_USER_AGENT'));

            Misc::redirect('indexAdmin');
        }
    }

    /**
     * Ovaj metod gasi sesiju cime efektivno unistava sve u sesiji,
     * a zatim preusmerava korisnika na stranicu za prijavu na login ruti.
     */
    function logout() {
        Session::end();
        Misc::redirect('login');
    }

    /**
     * Metod koji salje view-u spisak putnickih vozila za kategoriju za datim slug 
     * @param string $categorySlug
     */
    function listByCategory($categorySlug) {

        $category = CarCategoryModel::getBySlug($categorySlug);

        if (!$category) {
            Misc::redirect('');
        }
        $cars = CarModel::getCarsByCarCategoryId($category->car_category_id);

        for ($i = 0; $i < count($cars); $i++) {
            $cars[$i]->images = CarModel::getCarImages($cars[$i]->car_id);
            $cars[$i]->tags = CarModel::getTagsForCarId($cars[$i]->car_id);
        }
        $this->set('cars', $cars);
        $this->set('category', $category);
    }

    /**
     * Metod stranice koja prikazuje detalje o odabranom putnickom vozilu
     * @param string $slug Slug putnickog vozila - odgovara regularnom izrazu [a-z0-9\-]+
     */
    function car($slug) {
        $car = CarModel::getBySlug($slug);

        if (!$car) {
            Misc::redirect('');
        }

        $this->set('categories', CarCategoryModel::getAll());

        $car->images = CarModel::getCarImages($car->car_id);
        $car->tags = CarModel::getTagsForCarId($car->car_id);
        $car->location = LocationModel::getById($car->location_id);
        $car->car_category = CarCategoryModel::getById($car->car_category_id);

        $this->set('car', $car);
    }

    /**
     * Metod stranice koja prihvata parametre za pretragu iz filter formulara pocetne stranice
     */
    function search() {
        if (!$_POST) {
            Misc::redirect('');
        }

        $location_id = filter_input(INPUT_POST, 'location_id', FILTER_SANITIZE_NUMBER_INT);
        $car_category_id = filter_input(INPUT_POST, 'car_category_id', FILTER_SANITIZE_NUMBER_INT);

        $tag_ids = filter_input(INPUT_POST, 'tag_ids', FILTER_SANITIZE_NUMBER_INT, FILTER_REQUIRE_ARRAY);

        $cars = CarModel::homePageSearch($location_id, $car_category_id, $tag_ids);

        $this->set('categories', CarCategoryModel::getAll());
        $this->set('locations', LocationModel::getAll());
        $this->set('tags', TagModel::getAll());

        for ($i = 0; $i < count($cars); $i++) {
            $cars[$i]->images = CarModel::getCarImages($cars[$i]->car_id);
            $cars[$i]->tags = CarModel::getTagsForCarId($cars[$i]->car_id);
        }

        $this->set('cars', $cars);

        $this->set('location_id', $location_id);
        $this->set('car_category_id', $car_category_id);
        $this->set('tag_ids', $tag_ids);
    }

}
