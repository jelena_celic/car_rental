<?php
/**
 * Klasa kontrolera admin panela aplikacije za rad sa kategorijama putnickih vozila
 */

class AdminCarCategoryController extends AdminController {
     /**
     * Indeks metod admin kontrolera za rad sa kategorijama prikazuje spisak svih kategorija
     */
       public function index() {
        $this->set('categories', CarCategoryModel::getAll());
    }
 /**
     * Ovaj metod prikazuje formuilar za dodavanje ili vrsi dodavanje ako su podaci poslati HTTP POST metodom
     * @return void
     */
    
    public function add() {
        if (!$_POST) return;

        $name = filter_input(INPUT_POST, 'name');
        $slug = filter_input(INPUT_POST, 'slug');

        $location_id = CarCategoryModel::add($name, $slug);

        if ($location_id) {
            Misc::redirect('admin/categories/');
        } else {
            $this->set('message', 'Doslo je do greske prilikom dodavanja kategorije u bazu podataka.');
        }
    }
 /**
     * Ovaj metod prikazuje formuilar za izmenu ili vrsi izmenu ako su podaci poslati HTTP POST metodom
     * @return void
     */
    
    public function edit($id) {
        $category = CarCategoryModel::getById($id);

        if (!$category) {
            Misc::redirect('admin/categories/');
        }

        $this->set('category', $category);

        if (!$_POST) return;

        $name = filter_input(INPUT_POST, 'name');
        $slug = filter_input(INPUT_POST, 'slug');

        $res = CarCategoryModel::edit($id, $name, $slug);

        if ($res) {
            Misc::redirect('admin/categories/');
        } else {
            $this->set('message', 'Doslo je do greske prilikom izmene podataka o kategoriji.');
        }
    }
}
